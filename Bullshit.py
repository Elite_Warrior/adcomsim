import numpy as np

def summary(num):
    term1 = num/2
    term2 = num+1
    sum = term1*term2
    return sum

def sum_range(start, end):
    start = summary(start)
    end = summary(end)
    sum = end-start
    return sum

def add_range(start, end, steps):
    summary =0 
    for num in range(start, end, steps):
        summary += num
    return summary

def func():
    o = np.polyfit([0, 1, 2], [1,3,6], 2)
    return o

def term_calc(numbers, x):
    result = 0
    numbers = np.flip(numbers)
    for index in range(len(numbers)-1, -1, -1):
        # print(numbers[index], index)
        result += numbers[index]*(x**index)
    return result

def luck_level(level, start, adding):
    level = level-1
    chance = start
    if level == 0:
        return start
    start = 1.75
    sum = 0
    for index in range(1, level):
        sum += start+adding
    chance += sum
    return chance

if __name__ == "__main__":
    print('jo')
    chance = luck_level(4, 5, 0.5)
    print(chance)

#TEst