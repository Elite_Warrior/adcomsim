
class ErzeugerTime():

    def __init__(self, stunden=0, minuten=0, sekunden=0) -> None:
        self.stunden = stunden
        self.minuten = minuten
        self.sekunden = sekunden

    def get_ticks(self) -> int:
        ticks = 0
        ticks += self.stunden*60*60
        ticks += self.minuten*60
        ticks += self.sekunden
        return ticks
