from __future__ import annotations
from NumberParser import NumberParser
from numbers import Number
import copy

class ErzeugerFloat():

    def __init__(self, name, value, dps, strength=1, speed=1, chance=0, bonus=8, isDPU=False) -> None:
        self.name = name
        self.menge = NumberParser.convert_str_to_number(value)
        self.dps = NumberParser.convert_str_to_number(dps)
        self.strength = strength
        self.speed = speed
        self.chance = chance
        self.bonus = bonus
        self.calc_crit_multiplier(chance, bonus)
        if isDPU:
            self.recalc_dps(dps)
            self.dpu=dps
        else:
            self.recalc_dpu(dps)
            
    def recalc_dps(self, dpu=None):
        if dpu is not None:
            self.dps=dpu*self.strength*self.speed*self.menge
        else:
            self.dps=self.dpu*self.strength*self.speed*self.menge
        
    def recalc_dpu(self, dps):
        self.dpu=dps/self.strength/self.speed/self.menge
        
    def calc_crit_multiplier(self, chance, bonus):
        self.crit_multiplier = 1-chance+chance*bonus
        
    def copy(self):
        return copy.deepcopy(self)

    def add_erzeuger(self, other):
        self.menge += other.dps*other.crit_multiplier
        self.recalc_dps()

    def add_number(self, number):
        self.menge += number*self.crit_multiplier
        self.recalc_dps()

    def __iadd__(self, other) -> ErzeugerFloat:
        if isinstance(other, ErzeugerFloat):
            self.menge += other.dps
            self.recalc_dps()
        elif isinstance(other, Number):
            self.menge += other
            self.recalc_dps()
        else:
            raise TypeError
        return self

    def __str__(self) -> str:
        string = self.name + ': Menge ' + NumberParser.number_to_string(self.menge) + ', Output ' + NumberParser.number_to_string(self.dps)
        return string

if __name__ == "__main__":
    bauern = ErzeugerFloat('Bauern', '100', 300, chance=0.01, bonus=2)
    konvois = ErzeugerFloat('Konvois', 10, 15)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    print(konvois)
    
