from ErzeugerTime import ErzeugerTime
from Stage import Stage
from ErzeugerFloat import ErzeugerFloat
from events.Event import Event
from enum import Enum
import time
import events.MiniEvents as ME
import events.CustomEvent as CE
import events.BigEvents as BE
import events.MainGameEvent as BASE


class EventEnum(Enum):
    COWBOY = BE.Cowboy
    WINTER = BE.Winter
    KREUZZUG = BE.Kreuzzug
    EXPORT = ME.Export
    SCHILD = ME.Schild
    CUSTOM = CE.CustomEvent
    MAIN = BASE.MainGame


class NewSim():

    def __init__(self, enum: EventEnum) -> None:
        self.event = self.create_event_stages(enum)

    def create_event_stages(self, event_enum: EventEnum) -> Event:
        event = event_enum.value()
        return event

    def sim_tick(self, ticks=1, stage=None):
        if stage is None:
            stage = self.event
        for number in range(0, ticks):
            stage.tick()

    def sim_tick_reverse(self, ticks):
        for number in range(0, ticks):
            self.event.tick_wrong_order()

    def sim_poly_curve_tick(self, ticks=1):
        self.event.poly_curve_tick(ticks)

    def sim_log_tick(self, ticks):
        ez = []
        for number in range(0, ticks):
            self.event.tick()
            ez.append(self.event[0][3].menge)
        return ez

    def print_stage(self):
        print(self.stage)

    def print_event(self):
        print(self.event)


if __name__ == "__main__":
    """
    Nur sim tick ist genau. Die anderen haben einen Fehler
    """
    sim = NewSim(EventEnum.MAIN)
    # sim.event[0][1].set_dpu(multiplier=2)
    # sim.event[2][0].set_dpu(multiplier=2)
    # print(sim.event[2][1])
    start = time.process_time()
    # sim.print_event()
    # gametime = ErzeugerTime(1, 35, 0)
    gametime = ErzeugerTime(1, 39, 0)
    # sim.manual_tick(4)
    # sim.sim_tick_reverse(10004)
    print(gametime.get_ticks())
    sim.sim_tick(gametime.get_ticks())
    # sim.sim_poly_curve_tick(ticks)
    sim.print_event()
    print('Time Elapsed: ', time.process_time() - start)
