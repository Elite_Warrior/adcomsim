from __future__ import annotations
from numbers import Number

class ACNumber():

    def __init__(self, number = 0.0, exp = '') -> None:
        if isinstance(exp, str):
            self.thousandth = self.parse_expName_to_thousandth(exp)
        else:
            self.thousandth = exp
        self.number, self.thousandth = self.check_number_bound(number, self.thousandth)

    @staticmethod
    def parse_expName_to_thousandth(exp_name:str) -> int:
        exp_name = exp_name.strip().upper()
        if exp_name == '':
            exp = 0
        elif exp_name == 'M':
            exp = 2
        elif exp_name == 'B':
            exp = 3
        elif exp_name == 'T':
            exp = 4
        elif len(exp_name) == 2:
            exp = (ord(exp_name[0]) - 64) + 4
        elif len(exp_name) == 3:
            exp = (ord(exp_name[0]) - 64) + 30
        elif len(exp_name) == 4:
            exp = (ord(exp_name[0]) - 64) + 56
        else:
            exp = 0
        return exp

    @staticmethod
    def thousandth_to_str(exp):
        if exp < 2:
            exp_name = ''
        elif exp == 2:
            exp_name = 'M'
        elif exp == 3:
            exp_name = 'B'
        elif exp == 4:
            exp_name = 'T'
        elif exp <= 30: #4+26
            exp_name = chr(exp + 60) * 2
        elif exp <= 56: #4+52
            exp_name = chr(exp + 34) * 3
        else:
            exp_name = chr(exp + 8) * 4
        return exp_name

    @staticmethod
    def check_number_bound(number, exp):
        if number >= 1000:
            number /= 1000
            exp += 1
            number, exp = ACNumber.check_number_bound(number, exp)
        elif number < 1:
            number *= 1000
            exp -= 1
            number, exp = ACNumber.check_number_bound(number, exp)
        return number, exp

    @staticmethod
    def check_number_bound_new(number, exp):
        if number >= 1000:
            length = ACNumber.numerator_length(number)

        elif number < 1:
            pass
        return number, exp

    @staticmethod
    def numerator_length(number) -> int:
        number_str = '{0:.0f}'.format(number)
        numerator_length = len(number_str)
        return numerator_length

    @staticmethod
    def exponent_convert(numerator_length: int):
        exp = int((numerator_length-1)/3)
        return exp

    def get_full_number(self) -> int:
        full_number = self.number
        full_number = full_number*1000**self.thousandth
        return full_number

    def __add__(self, other) -> ACNumber:
        number = self.number
        if isinstance(other, Number):
            number += other
        elif isinstance(other, ACNumber):
            exp_diff = self.thousandth-other.thousandth
            if exp_diff > 0:
                number += other.number/(1000**exp_diff)
            elif exp_diff < 0:
                number += other.number*(1000**abs(exp_diff))
            else:
                number += other.number
        number, thousandth = self.check_number_bound(number, self.thousandth)
        new_acnumber = ACNumber(number, thousandth)
        return new_acnumber

    __radd__=__add__    

    def __mul__(self, other) -> ACNumber:
        number = self.number
        thousandth = self.thousandth
        if isinstance(other, Number):
            number *= other
        elif isinstance(other, self.__class__):
            number *= other.number
            thousandth += other.thousandth
        else:
            raise TypeError
        number, thousandth = self.check_number_bound(number, thousandth)
        new_acnumber = ACNumber(number, thousandth)
        return new_acnumber

    def __truediv__(self, other) -> ACNumber:
        number = self.number
        thousandth = self.thousandth
        if isinstance(other, Number):
            number /= other
        if isinstance(other, self.__class__):
            number /= other.number
            thousandth -= other.thousandth
        else:
            raise TypeError
        number, thousandth = self.check_number_bound(number, thousandth)
        new_acnumber = ACNumber(number, thousandth)
        return new_acnumber

    def __str__(self):
        if self.thousandth < 1:
            string = "{:.0f}".format(self.number)
        elif self.thousandth < 2:
            string = "{:.0f}".format(self.number)+"000"
        else:
            string = "{:3.2f}".format(self.number)+ ' ' +self.thousandth_to_str(self.thousandth)
        return string

if __name__ == "__main__":
    base = ACNumber(100, 'AA')
    base2 = ACNumber(50,'BB')
    base3 = ACNumber(10,'BB')
    base4 = ACNumber(5000000000)
    base5 = ACNumber(438.87564309,'ZZZZ')
    #print(base + base2 + base3)
    #print(base + base2 + base3)
    print(base * base4)
    #print(base.numerator_length(123276894764597645680943069850.325345))
    length = base.numerator_length(1281222.4123)
    print(base.exponent_convert(length))
    print(base4.get_full_number())
    print(base5.get_full_number())
