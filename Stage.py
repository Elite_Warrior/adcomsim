from Bullshit import term_calc
from ErzeugerFloat import ErzeugerFloat
import numpy as np

class Stage():
    
    def __init__(self, name, production=1, luck=0, luck_strength=3) -> None:
        self.name = name
        self.erzeuger_array = []
        self.production = production
        self.luck = luck
        self.luck_strength = luck_strength
        self.multiplikator = 1

    def recalc_multiplikator(self, event_multiplikator, event_luck):
        luck = self.luck+event_luck
        multiplikator = (1-luck+self.luck_strength*luck)
        multiplikator = multiplikator*event_multiplikator
        self.multiplikator = multiplikator
        
    def append(self, value):
        self.erzeuger_array.append(value)

    def __setitem__(self, key, value):
        self.erzeuger_array[key] = value

    def __getitem__(self, key: int) -> ErzeugerFloat:
        return self.erzeuger_array[key]

    def __len__(self) -> int:
        return len(self.erzeuger_array)

    def tick(self): # von hinten nach vorne gerechnet
        for index in range(len(self.erzeuger_array)-1, 0, -1): # wird Rückwärts durchlaufen
            erzeuger1 = self.erzeuger_array[index-1]
            erzeuger2 = self.erzeuger_array[index]
            erzeuger1.add_erzeuger(erzeuger2)

    def tick_wrong_order(self):
        for index in range(0, len(self.erzeuger_array)-1): # wird Rückwärts durchlaufen
            erzeuger1 = self.erzeuger_array[index]
            erzeuger2 = self.erzeuger_array[index+1]
            erzeuger1.add_erzeuger(erzeuger2)

    def poly_curve_tick(self, ticks):
        stage_length = len(self.erzeuger_array)
        array_x = [i for i in range(0, stage_length)]
        array = [None]*stage_length
        for index in range(0, stage_length):
            array = self.copy_stage_to_list(array)
            self.tick()
        for index in range(0, stage_length):
            inv_index = stage_length-index
            poly = np.polyfit(array_x,array[index], inv_index-1) # np.clip(inv_index,0,3)  inv_index-1
            output = term_calc(poly, ticks)
            self.erzeuger_array[index].menge = output
            self.erzeuger_array[index].recalc_dps()

    def copy_stage_to_list(self, array: list):
        for index in range(0, len(self.erzeuger_array)):
            if array[index] == None:
                array[index] = []
            array[index].append(self.erzeuger_array[index].menge)
        return array

    def term_calc(numbers, x):
        result = 0
        numbers = np.flip(numbers)
        for index in range(len(numbers)-1, -1, -1):
            print(numbers[index], index)
            result += numbers[index]*(x**index)
        return result

    def __str__(self):
        string = '-Stage ' + self.name + '-'
        for erzeuger in self.erzeuger_array:
            string+='\n'
            string+=str(erzeuger)
        return string 