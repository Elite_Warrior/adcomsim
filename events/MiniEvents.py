from events.Event import Event
from Stage import Stage
from ErzeugerFloat import ErzeugerFloat


class Export(Event):
    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage =  list()
        stage.append(Stage('Kartoffel'))
        stage.append(Stage('Kisten'))
        stage[0].append(ErzeugerFloat('Bauern', 1, 2))
        stage[0].append(ErzeugerFloat('Pflanzungen', 1, 1))
        stage[0].append(ErzeugerFloat('Bewaesserung', 1, 0.444))
        stage[0].append(ErzeugerFloat('Spruehflugzeuge', 1, 0.185))
        #----
        stage[1].append(ErzeugerFloat('Frachten', 1, 6))
        stage[1].append(ErzeugerFloat('Zuege', 1, 3))
        stage[1].append(ErzeugerFloat('Frachtkahne', 1, 1.33))
        stage[1].append(ErzeugerFloat('Haefen', 1, 0.556))
        return stage

class Schild(Event):
    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage =  list()
        stage.append(Stage('Munition'))
        stage.append(Stage('Bandagen'))
        stage[0].append(ErzeugerFloat('Soldaten', 1, 2))
        stage[0].append(ErzeugerFloat('Bunker', 1, 1))
        stage[0].append(ErzeugerFloat('Artillerie', 1, 0.444))
        stage[0].append(ErzeugerFloat('Forts', 1, 0.185))
        #----
        stage[1].append(ErzeugerFloat('Krankenschwester', 1, 6))
        stage[1].append(ErzeugerFloat('Klinik', 1, 3))
        stage[1].append(ErzeugerFloat('Hospital', 1, 1.33))
        stage[1].append(ErzeugerFloat('Küstenwache', 1, 0.556))
        return stage