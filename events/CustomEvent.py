from Stage import Stage
from events.Event import Event
from ErzeugerFloat import ErzeugerFloat

class CustomEvent(Event):

    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage =  list()
        stage.append(Stage('Kartoffel'))
        sta = stage[0]
        sta.append(ErzeugerFloat('Bauern', 1000, 2000))
        stage[0].append(ErzeugerFloat('Pflanzungen', 100, 300))
        stage[0].append(ErzeugerFloat('Bewaesserung', 10, 30))
        stage[0].append(ErzeugerFloat('Spruehflugzeuge', 1, 1))
        return stage