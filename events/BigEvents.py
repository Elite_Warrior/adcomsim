from Stage import Stage
from events.Event import Event
from ErzeugerFloat import ErzeugerFloat

class Cowboy(Event):
    
    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage = list()
        stage.append(Stage('Vieh'))
        stage.append(Stage('Eisenbahnen'))
        stage.append(Stage('Revolver'))
        sta = stage[0]
        sta.append(ErzeugerFloat('Cowboys', 1, 1.5))
        stage[0].append(ErzeugerFloat('Pferde', 1, 1.25))
        stage[0].append(ErzeugerFloat('Karren', 1, 0.875))
        stage[0].append(ErzeugerFloat('Glueheisen', 1, 0.563))
        stage[0].append(ErzeugerFloat('Karavane', 1, 0.344))
        stage[0].append(ErzeugerFloat('Forts', 1, 0.203))
        return stage

class Winter(Event):
    
    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage = list()
        stage.append(Stage('Kekse'))
        stage.append(Stage('Schaufel'))
        stage.append(Stage('Schneebälle'))
        stage[0].append(ErzeugerFloat('Bäcker', 1, 2.5))
        stage[0].append(ErzeugerFloat('Rezepte', 1, 1.75))
        stage[0].append(ErzeugerFloat('Mixer', 1, 1.13))
        stage[0].append(ErzeugerFloat('Öfen', 1, 0.688))
        stage[0].append(ErzeugerFloat('Backstuben', 1, 0.406))
        stage[0].append(ErzeugerFloat('Keksfabrik', 1, 0.234))
        return stage


class Kreuzzug(Event):
    
    def __init__(self) -> None:
        super().__init__()

    def fill_stages(self) -> list:
        stage = list()
        stage.append(Stage('Hähnchenschenkel'))
        stage.append(Stage('Magie'))
        stage.append(Stage('Schwerter'))
        stage[0].append(ErzeugerFloat('Bauern', 1, 1))
        stage[0].append(ErzeugerFloat('Ställe', 1, 0.5))
        stage[0].append(ErzeugerFloat('Dörfer', 1, 0.222))
        stage[0].append(ErzeugerFloat('Türme', 1, 0.0926))
        stage[0].append(ErzeugerFloat('Sümpfe', 1, 0.0370))
        stage[0].append(ErzeugerFloat('Burgen', 1, 0.0144))
        stage[2].append(ErzeugerFloat('Knappen', 1, 1))
        stage[2].append(ErzeugerFloat('Ritter', 1, 0.375))
        stage[2].append(ErzeugerFloat('Trebuchets', 1, 0.0972))
        stage[2].append(ErzeugerFloat('Drachen', 1, 0.0220))
        return stage