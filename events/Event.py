from Stage import Stage

class Event():

    def __init__(self, production=1, luck=0, luck_strength=8) -> None:
        self.stage = self.fill_stages()
        self.production = production
        self.luck = luck
        self.luck_strength = luck_strength

    def fill_stages(self) -> list:
        raise NotImplementedError

    def __str__(self) -> str:
        string='Event: ' + self.__class__.__name__
        for stage in self.stage:
            string += '\n\t'
            string += str(stage).replace('\n','\n--> ')
        return string

    def __getitem__(self, key: int) -> Stage:
        return self.stage[key]
            
    def tick(self):
        for stage in self.stage:
            stage.tick()

    def tick_wrong_order(self):
        for stage in self.stage:
            stage.tick_wrong_order()

    def poly_curve_tick(self, ticks):
        if ticks < 100:
            for index in range(0, ticks):
                self.tick()
        for stage in self.stage:
            stage.poly_curve_tick(ticks)