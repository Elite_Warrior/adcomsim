from ErzeugerFloat import ErzeugerFloat
from Stage import Stage
from events.Event import Event


class MainGame(Event):

    def __init__(self) -> None:
        super().__init__()
        
    def fill_stages(self) -> list:
        stage = list()
        stage.append(Stage('Placebos'))
        strength = 4096
        chance = 0.4525
        bonus = 8192
        stage[0].append(ErzeugerFloat('Placebos',0 ,0, isDPU=True))
        stage[0].append(ErzeugerFloat('Krankenschwester', '387,45 UU', 1.166666666666667, strength, 256, chance, bonus, True))
        stage[0].append(ErzeugerFloat('Klinik', '5,06 QQ', 0.666666666666667, strength, 256, chance, bonus, True))
        stage[0].append(ErzeugerFloat('Krankenwagen', '111,65 LL', 0.375, strength, 256, chance, bonus, True))
        stage[0].append(ErzeugerFloat('Apotheke', '4,03 HH', 0.208333333333333, strength, 256, chance, bonus, True))
        stage[0].append(ErzeugerFloat('Blutbank', '223,71CC', 0.114583333333333, strength, 256, chance, bonus, True))
        stage[0].append(ErzeugerFloat('Hospital', '19,26B', 0.0625, strength, 256, chance, bonus, True))
        #stage[0].append(ErzeugerFloat('Kuestenwache', '0', 0.0338541666666667, strength, 256, chance, bonus, True))
        return stage