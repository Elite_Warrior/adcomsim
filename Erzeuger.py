from __future__ import annotations
from ACNumber import ACNumber

class Erzeuger():

    def __init__(self, name, value, dps) -> None:
        self.name = name
        number, exp = self.parse_units(value)
        self.base = ACNumber(number, exp)
        number, exp = self.parse_units(dps)
        self.dps = ACNumber(number, exp)
        self.dpu = self.dps/self.base
        self.dpu = self.dpu.get_full_number()

    @staticmethod
    def parse_units(string):
        string = str(string)
        string = string.replace(' ','').replace(',','.').strip().upper()
        index = Erzeuger.has_alphabet(string)
        if index != -1:
            number = float(string[:index])
            exp = string[index:]
        else:
            number = float(string)
            exp = ''
        return number, exp

    @staticmethod
    def has_alphabet(string):
        for index, element in enumerate(string, 0):
            if element.isalpha() and element != '.':
                return index
        return -1

    def __iadd__(self, other) -> Erzeuger:
        self.base += other.dps
        self.dps += other.dps*self.dpu
        return self

    def __str__(self) -> str:
        string = self.name + ': value ' + str(self.base) + ', Output ' + str(self.dps)
        return string

if __name__ == "__main__":
    bauern = Erzeuger('Bauern', 100, 300)
    konvois = Erzeuger('Konvois', 10, 15)
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    bauern += konvois
    print(bauern, bauern.dpu)
    print(konvois)