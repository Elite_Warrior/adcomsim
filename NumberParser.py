class NumberParser():

    @staticmethod
    def convert_str_to_number(number_str) -> float:
        number_str = str(number_str)
        number, exp = NumberParser.parse_units(number_str)
        number = number*NumberParser.parse_exp_str_to_faktor(exp)
        return number

    @staticmethod
    def parse_units(string):
        string = str(string)
        string = string.replace(' ','').replace(',','.').strip().upper()
        index = NumberParser.has_alphabet(string)
        if index != -1:
            number = float(string[:index])
            exp = string[index:]
        else:
            number = float(string)
            exp = ''
        return number, exp

    @staticmethod
    def has_alphabet(string):
        for index, element in enumerate(string, 0):
            if element.isalpha() and element != '.':
                return index
        return -1

    @staticmethod
    def number_to_string(number):
        number_str = '{0:f}'.format(number)
        number_str = number_str.split('.')[0]
        number_length = len(number_str)
        if number_length <= 6:
            return number_str
        elif number_length > 6 and number_length <=9:
            number_exp = ' m'
        elif number_length > 9 and number_length <=12:
            number_exp = ' b'
        elif number_length > 12 and number_length <=15:
            number_exp = ' t'
        else:
            number_length_devided = int((number_length-16)/3)
            if number_length_devided<=25:
                number_exp = ' '+chr(number_length_devided+97)*2
            else:
                number_exp = ' '+chr(number_length_devided-26+97)*3
        mod_len = number_length%3
        if mod_len == 0:
            mod_len = 3
        number_digit = number_str[:mod_len]+','+number_str[mod_len:mod_len+2]
        return number_digit+number_exp.upper()

    @staticmethod
    def parse_exp_str_to_faktor(exp_str:str) -> int:
        exp_str = exp_str.strip().upper()
        if exp_str == '':
            exp = 0
        elif exp_str == 'M':
            exp = 2
        elif exp_str == 'B':
            exp = 3
        elif exp_str == 'T':
            exp = 4
        elif len(exp_str) == 2:
            exp = (ord(exp_str[0]) - 64) + 4
        elif len(exp_str) == 3:
            exp = (ord(exp_str[0]) - 64) + 30
        elif len(exp_str) == 4:
            exp = (ord(exp_str[0]) - 64) + 56
        else:
            exp = 0
        exp = 1000**exp
        return exp

    