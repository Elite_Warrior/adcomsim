from decimal import Decimal

def string_to_number(string: str):
    string = string.replace(' ','').replace(',','.').strip().lower()
    for index, element in enumerate(string, 0):
        if element.isalpha() and element != '.':
            number = Decimal(string[:index])
            exponent_str = string[index:]
            if exponent_str == 'm':
                exponent = 6
            elif exponent_str == 'b':
                exponent = 9
            elif exponent_str == 't':
                exponent = 12
            else:
                if len(exponent_str) == 2:
                    exponent = (ord(exponent_str[1]) - 96) * 3 + 12
                else:
                    exponent = (ord(exponent_str[1]) - 96) * 3 + 37
            exponent = Decimal(10 ** exponent)
            number = number * exponent
            return number
    return Decimal(string.replace(',','.'))

def number_to_string(number):
    number_str = '{0:f}'.format(number)
    number_str = number_str.split('.')[0]
    number_length = len(number_str)
    if number_length <= 6:
        return number_str
    elif number_length > 6 and number_length <=9:
        number_exp = ' m'
    elif number_length > 9 and number_length <=12:
        number_exp = ' b'
    elif number_length > 12 and number_length <=15:
        number_exp = ' t'
    else:
        number_length_devided = int((number_length-16)/3)
        if number_length_devided<=25:
            number_exp = ' '+chr(number_length_devided+97)*2
        else:
            number_exp = ' '+chr(number_length_devided-26+97)*3
    mod_len = number_length%3
    if mod_len == 0:
        mod_len = 3
    number_digit = number_str[:mod_len]+','+number_str[mod_len:mod_len+2]
    return number_digit+number_exp.upper()


class Erzeuger():

    def __init__(self, name, einheiten, dps) -> None:
        self.name = name
        self.einheiten = string_to_number(einheiten)
        self.dps = string_to_number(dps)
        self.dps = self.dps/self.einheiten
    
    def add(self, add):
        self.einheiten += add

    def get_total(self):
        total = self.einheiten * self.dps
        return total

    def print_total(self, mul = 1):
        number = self.get_total()*mul
        print(number_to_string(number))


if __name__ == "__main__":
    #knappe = Erzeuger("Knappe", "6,75AA", "24,19BB")
    #knappe.print_total(100)
    knappe = Erzeuger("Test", "1xx", "1aa")
    knappe.print_total(1)
    #print(ord('z'))
    #print(string_to_number("1,2AA"))
    #print(number_to_string(153100100100100))

#a = Decimal(1650641804030154013140350150430)
#print(a)
#1000000000000000 # 1AA
#2000000000000000000000
#1000000000000000000